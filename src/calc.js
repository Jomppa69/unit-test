
/**
 * Adds two numbers together
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number}
 */
const add = (a, b) => a + b;

const subtract = (minuend, subtrahend) => {
    return minuend - subtrahend;
};

const multiply = (multiplier, multiplicant) => {
    return multiplier * multiplicant;
};

/**
 * Divides tow numbers.
 * @param {Number} divident 
 * @param {Number} divisor 
 * @returns {Number}
 * @throws {Error} 0 division
 */
const divide = (divident, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed")
        const fraction = divident / divisor;
        return fraction;
};
export default {add, subtract, divide, multiply}
