git init
npm init -y

npm install --save express
npm install --save-dev mocha chai

echo "node_modules" > .gitignore

git remote add origin "https://gitlab.com/Jomppa69/unit-test"
git remote set-url origin #vaihda osoite
git push -u origin main

touch readme.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

node src/main.js